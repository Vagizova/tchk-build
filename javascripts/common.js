'use strict';

function scrollToNextSection(element, to, duration) {
    if (duration <= 0) return;
    var difference = to - element.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(function () {
        if (element.scrollTop === to) return;
        element.scrollTop = element.scrollTop + perTick;
        scrollToNextSection(element, to, duration - 10);
    }, 10);
}

var nextItem = document.getElementById('js-target');
var curBtn = document.getElementById('js-next-btn');

curBtn.addEventListener('click', function (e) {
    e.preventDefault();
    scrollToNextSection(document.documentElement, nextItem.offsetTop, 1000);
});
//# sourceMappingURL=../javascripts/common.js.map
